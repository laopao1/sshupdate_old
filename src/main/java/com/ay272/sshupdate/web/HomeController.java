package com.ay272.sshupdate.web;

import com.ay272.sshupdate.entity.User;
import org.springframework.stereotype.Controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

/**
 * Created by java on 2016/5/24.
 */
@Controller
@RequestMapping("home")
public class HomeController {
    @RequestMapping("/index")
    public String index(){
        System.out.println("访问网址：http://localhost:8080/sshupdate2/home/index.action");
        System.out.println("响应成功");
        return "home";
    }
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String save(User user, RedirectAttributesModelMap model){
        System.out.println("信息");
        System.out.println(user);
        model.addFlashAttribute("u",user);
        return "redirect:show.action";
    }
    @RequestMapping("show")
    public String show(){
        return "show";
    }
}

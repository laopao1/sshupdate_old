package com.ay272.sshupdate.config;


import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;


/**
 * Created by java on 2016/5/24.
 */
public class Boot  extends AbstractAnnotationConfigDispatcherServletInitializer{
   /*
     加载以前applicationcontext.xml
    */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{AppConfig.class};
    }
    /*
    * 等同于以前的web.xml中配置的Server
    *
    * */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"*.action"};
    }

    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter("UTF-8");

        return new Filter[]{encodingFilter};
    }
}

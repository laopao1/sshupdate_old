package com.ay272.sshupdate.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

/**
 * 等同于加载以前的applicationcontext.xml
 * Created by java on 2016/5/24.
 */
@Configuration
@ComponentScan(basePackages ="com.ay272.sshupdate",excludeFilters = {@ComponentScan.Filter(value ={Controller.class})})
public class AppConfig {
}

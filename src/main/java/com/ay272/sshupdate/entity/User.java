package com.ay272.sshupdate.entity;

import java.io.Serializable;

/**
 * Created by java on 2016/5/24.
 */
public class User implements Serializable{
    private int agg;
    private String name;

    public int getAgg() {
        return agg;
    }

    @Override
    public String toString() {
        return name+"---"+agg;
    }

    public void setAgg(int agg) {
        this.agg = agg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
